const fetch = require('isomorphic-unfetch');
const cheerio = require('cheerio');
const util = require('util');
const exec = util.promisify(require('child_process').exec);

async function downloadPackages(count, callback) {
  const html = await fetch('https://www.npmjs.com/browse/depended').then(res =>
    res.text()
  );
  const $ = cheerio.load(html);
  const pkgs = [];
  $('a > h3')
    .slice(0, count)
    .each(function() {
      pkgs.push($(this).text());
    });
  await exec('rm -rf ./packages  && mkdir ./packages');
  console.log(pkgs);
  await Promise.all(
    pkgs.map(pkg =>
      exec(`npm v ${pkg} dist.tarball | xargs curl | tar -xz && mkdir ./packages/${pkg} && mv package/* packages/${pkg}
    `)
    )
  )
}
//&& find ./packages/${pkg} -regex '^\.' -delete
// downloadPackages(5);
module.exports = downloadPackages
